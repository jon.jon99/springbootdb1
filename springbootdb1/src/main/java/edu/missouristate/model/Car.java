package edu.missouristate.model;

public class Car {
	
	private Integer numberOfWheels;
	
	public Car() {
	}

	public Integer getNumberOfWheels() {
		return numberOfWheels;
	}

	public void setNumberOfWheels(Integer numberOfWheels) {
		this.numberOfWheels = numberOfWheels;
	}

}
