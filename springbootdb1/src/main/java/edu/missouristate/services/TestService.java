package edu.missouristate.services;

public interface TestService {

	public boolean isAuthenticated(String id, String password);

}
